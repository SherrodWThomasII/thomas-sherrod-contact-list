﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThomasSherrod_ContactList
{
    public partial class Contacts : Form
    {
        public event EventHandler friends;
        public Contacts()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public ContactData contactData
        {
            get
            {
                ContactData contact = new ContactData();
                contact.FirstName = txtFirstName.Text;
                contact.LastName = txtLastName.Text;
                contact.Email1 = txtEmail.Text;
                contact.PhoneNumber = txtPhoneNumber.Text;
                return contact;
            }
            set
            {
                txtFirstName.Text = value.FirstName;
                txtLastName.Text = value.LastName;
                txtEmail.Text = value.Email1;
                txtPhoneNumber.Text = value.PhoneNumber;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(friends != null)
            {
                friends(this, new EventArgs());
            }
            this.Close();
        }
    }
}
