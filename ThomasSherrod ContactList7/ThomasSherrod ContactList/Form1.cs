﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThomasSherrod_ContactList
{
    public partial class Form1 : Form
    {
        public event EventHandler popUp;
        public Form1()
        {
            InitializeComponent();
        }

        private void contactsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Contacts addContact = new Contacts();
            addContact.friends += AddContact_friends;
            addContact.Show();
        }

        private void AddContact_friends(object sender, EventArgs e)
        { // This is when the user presses the add button
            ListViewItem listViews = new ListViewItem();
            ContactData cData = (sender as Contacts).contactData;
            Contacts contact = new Contacts();
            listViews.Text = cData.ToString(); // Adds the test
            listViews.Tag = cData;
            lstContacts.Items.Add(listViews);
            if(contact.btnAdd != null)
            {
                listViews.ImageIndex = 0; // Adds the image
            }

        }

        private void lstContacts_DoubleClick(object sender, EventArgs e)
        {
            Contacts dClick = new Contacts();
            popUp += dClick.ContactInfo;
            dClick.Edit += DClick_Edit;
            dClick.Show();
            if(popUp != null)
            {
                popUp(lstContacts.SelectedItems[0].Tag, EventArgs.Empty);
            }
        }

        private void DClick_Edit(object sender, Contacts.EditContacts e)
        {
            lstContacts.SelectedItems[0].Text = e.Person.Text;
        }

        private void largeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lstContacts.View = 0;
            largeToolStripMenuItem.Checked = true;
            smallToolStripMenuItem.Checked = false;
        }

        private void smallToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lstContacts.View = (View)2;
            smallToolStripMenuItem.Checked = true;
            largeToolStripMenuItem.Checked = false;
        }
    }
}
