﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThomasSherrod_ContactList
{
    public partial class Contacts : Form
    {
        public event EventHandler friends;
        public event EventHandler<EditContacts> Edit;
        public Contacts()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close(); // closes the form
        }

        private void btnEdit_Click(object sender, EventArgs e)
        { // This edits the double clicked contact.
            ListViewItem EditContacts = new ListViewItem();
            EditContacts.Text = contactData.ToString(); // Edits the text
            EditContacts.Tag = contactData;
            Edit(this, new EditContacts(EditContacts)); // Makes whatever the user changes the new contact.
        }
        public ContactData contactData
        { // gets and sets the data from contact data
            get
            {
                ContactData contact = new ContactData();
                contact.FirstName = txtFirstName.Text;
                contact.LastName = txtLastName.Text;
                contact.Email1 = txtEmail.Text;
                contact.PhoneNumber = txtPhoneNumber.Text;
                return contact;
            }
            set
            {
                txtFirstName.Text = value.FirstName;
                txtLastName.Text = value.LastName;
                txtEmail.Text = value.Email1;
                txtPhoneNumber.Text = value.PhoneNumber;
            }
        }
        public void ContactInfo (object sender, EventArgs e)
        { //gets the data from contact data.
            ContactData clickData = new ContactData();
            clickData.FirstName = txtFirstName.Text;
            clickData.LastName = txtLastName.Text;
            clickData.Email1 = txtEmail.Text;
            clickData.PhoneNumber = txtPhoneNumber.Text;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(friends != null)
            {
                friends(this, new EventArgs()); // Adds to the listview
            }
            this.Close();
        }
        public class EditContacts : EventArgs
        {
            ListViewItem person;
            public EditContacts(ListViewItem people)
            {
                Person = people;
            }

            public ListViewItem Person
            {
                get
                {
                    return person;
                }

                set
                {
                    person = value;
                }
            }
        }
    }
}
