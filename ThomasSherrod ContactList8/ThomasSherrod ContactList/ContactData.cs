﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThomasSherrod_ContactList
{
   public class ContactData
    { // Variables that will be used for the user entries
        string firstName;
        string lastName;
        string phoneNumber;
        string Email;
        int imageIndex;

        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                lastName = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return phoneNumber;
            }

            set
            {
                phoneNumber = value;
            }
        }

        public string Email1
        {
            get
            {
                return Email;
            }

            set
            {
                Email = value;
            }
        }

        public int ImageIndex
        {
            get
            {
                return imageIndex;
            }

            set
            {
                imageIndex = value;
            }
        }
        public override string ToString()
        {
            // What will display in the listview.
            return FirstName + " " + LastName + " " + PhoneNumber + " " + Email1;
        }
    }
}
